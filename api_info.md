# Example GraphQL Queries & Mutations

## Queries

### Get all

```
{
  destinations {
    id
    name
    country
    warm
    popularity
    rating
  }
}
```

### Get one by id

```
{
  destination(id: "5de6ab95bd03b9b2c64b0122") {
    id
    name
    country
    warm
  }
}
```

### Get warm destinations

```
{
  warmDestinations {
    id
    name
    country
  }
}
```

### Get popular destinations

```
{
  popularDestinations {
    id
    name
    country
    rating
  }
}
```

## Mutations

### Add new

```
mutation addNew {
  addNew(name: "Bondi", country: "Australia", warm: true, expensive: false, popularity: 2, rating: 2.3) {
    id
  }
}
```

### Update one

```
mutation update {
  update(id: "5de7e3dd0bfb14565dcad904", name: "Bondi Beach", country: "Australia", warm: true, expensive: false, popularity: 2, rating: 2.2) {
    id
  }
}
```

### Delete one

```
mutation delete {
  delete(id: "5de7e3dd0bfb14565dcad904") {
    id
  }
}
```
