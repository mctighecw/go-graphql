# README

My third project using Go (_Golang_). It is a basic web server using the Go package _net/http_, without any web framework. The Gorilla toolkit _mux_ package is also used for routing. It is connected to a MongoDB database.

There is a basic GraphQL API to add, get, update, and delete items in the _travel_ database, _destinations_ collection.

## App Information

App Name: go-graphql

Created: December 2019

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/go-graphql)

## Tech Stack

- Go
- [Gorilla mux](https://www.gorillatoolkit.org/pkg/mux)
- GraphQL
- Go Graphiql Package
- Go Mongo Driver
- MongoDB
- Docker

## To Run

### Setup & Run via Terminal

1. Install Go locally
2. Set `$GOPATH`
3. Install dependencies:

```
$ cd go-graphql
$ go get -u github.com/friendsofgo/graphiql
$ go get -u github.com/gorilla/mux
$ go get -u github.com/graphql-go/graphql
$ go get -u go.mongodb.org/mongo-driver/bson
$ go get -u go.mongodb.org/mongo-driver/mongo
```

3. Initialize database

```
$ go run ./src/scripts/init_db.go
```

4. Drop database, if necessary (later on)

```
$ go run ./src/scripts/drop_db.go
```

5. Start server

```
$ source run_dev.sh
```

Graphiql interface available at `http://localhost:9600/graphiql`

### With Docker

Add .env file with necessary variables to project root

```
$ docker-compose up -d --build
 (set up admin account)
$ source init_docker_db.sh
```

## GraphQL Queries & Mutations

See `api_info.md` for examples.

Last updated: 2024-11-18
