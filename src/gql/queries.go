package gql

import (
  "../db"
  "../lib"
  "../models"

  "github.com/graphql-go/graphql"
  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/bson/primitive"
)

func searchDestinations(query primitive.M) (interface{}, error) {
  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollName)

  p := models.Destination{}
  res := []models.Destination{}
  cursor, err := coll.Find(ctx, query)
  lib.LogError(err)

  for cursor.Next(ctx) {
    cursor.Decode(&p)
    res = append(res, p)
  }
  return res, nil
}

func CreateRootQuery() graphql.ObjectConfig {
  fields := graphql.Fields{
    "destinations": &graphql.Field{
      Type: graphql.NewList(DestinationType),
      Description: "Get all destinations",
      Resolve: func(params graphql.ResolveParams) (interface{}, error) {
        q := bson.M{}
        res, err := searchDestinations(q)

        return res, err
      },
    },
    "destination": &graphql.Field{
      Type: DestinationType,
      Description: "Get one destination by id",
      Args: DestinationArgs,
      Resolve: func(p graphql.ResolveParams) (interface{}, error) {
        id, _ := p.Args["id"].(string)

        client, ctx := db.DbConnect()
        db := client.Database(DbName)
        coll := db.Collection(CollName)

        objID, err := primitive.ObjectIDFromHex(id)
        res := coll.FindOne(ctx, bson.M{"_id": objID})
        err = res.Err()
        lib.LogError(err)

        if err != nil {
          return nil, nil
        } else {
          d := models.Destination{}
          res.Decode(&d)
          return d, nil
        }
      },
    },
    "warmDestinations": &graphql.Field{
      Type: graphql.NewList(DestinationType),
      Description: "Get all warm destinations",
      Resolve: func(params graphql.ResolveParams) (interface{}, error) {
        q := bson.M{"warm": true}
        res, err := searchDestinations(q)

        return res, err
      },
    },
    "popularDestinations": &graphql.Field{
      Type: graphql.NewList(DestinationType),
      Description: "Get all popular destinations (rating below 2.0)",
      Resolve: func(params graphql.ResolveParams) (interface{}, error) {
        val := 2.0
        q := bson.M{"rating": bson.M{"$lt": val}}
        res, err := searchDestinations(q)

        return res, err
      },
    },
  }

  rootQuery := graphql.ObjectConfig{Name: "RootQuery", Fields: fields}
  return rootQuery
}
