package gql

import (
  "../lib"

  "github.com/graphql-go/graphql"
)

func CreateSchema() graphql.Schema {
  rootQuery := CreateRootQuery()
  rootMutation := CreateRootMutation()

  schemaConfig := graphql.SchemaConfig{
    Query: graphql.NewObject(rootQuery),
    Mutation: graphql.NewObject(rootMutation),
  }
  schema, err := graphql.NewSchema(schemaConfig)
  lib.LogError(err)

  return schema
}
