package gql

import (
  "../db"
  "../lib"
  "../models"

  "github.com/graphql-go/graphql"
  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/bson/primitive"
)

func CreateRootMutation() graphql.ObjectConfig {
  fields := graphql.Fields{
    "addNew": &graphql.Field{
      Type: DestinationType,
      Description: "Add a new destination",
      Args: DestinationArgs,
      Resolve: func(params graphql.ResolveParams) (interface{}, error) {
        name, _ := params.Args["name"].(string)
        country, _ := params.Args["country"].(string)
        warm, _ := params.Args["warm"].(bool)
        expensive, _ := params.Args["expensive"].(bool)
        popularity, _ := params.Args["popularity"].(int)
        rating, _ := params.Args["rating"].(float64)

        client, ctx := db.DbConnect()
        db := client.Database(DbName)
        coll := db.Collection(CollName)

        d := models.Destination{
          ID: primitive.NewObjectID(),
          Name: name,
          Country: country,
          Warm: warm,
          Expensive: expensive,
          Popularity: popularity,
          Rating: rating,
        }

        _, err := coll.InsertOne(ctx, d)
        lib.LogError(err)

        return d, nil
      },
    },
    "update": &graphql.Field{
      Type: DestinationType,
      Description: "Update an existing destination",
      Args: DestinationArgs,
      Resolve: func(params graphql.ResolveParams) (interface{}, error) {
        // TO DO: check only present fields
        id, _ := params.Args["id"].(string)
        name, _ := params.Args["name"].(string)
        country, _ := params.Args["country"].(string)
        warm, _ := params.Args["warm"].(bool)
        expensive, _ := params.Args["expensive"].(bool)
        popularity, _ := params.Args["popularity"].(int)
        rating, _ := params.Args["rating"].(float64)

        client, ctx := db.DbConnect()
        db := client.Database(DbName)
        coll := db.Collection(CollName)
        objID, err := primitive.ObjectIDFromHex(id)

        updateRes, err := coll.UpdateOne(
          ctx,
          bson.M{"_id": objID},
          bson.M{
            "$set": bson.M{
              "name": name,
              "country": country,
              "warm": warm,
              "expensive": expensive,
              "popularity": popularity,
              "rating": rating,
            },
          },
        )

        modCount := updateRes.ModifiedCount
        lib.ErrorPanic(err)

        if (modCount == 0) {
          return nil, nil
        } else {
          res := coll.FindOne(ctx, bson.M{"_id": objID})
          d := models.Destination{}
          res.Decode(&d)
          return d, nil
        }
      },
    },
    "delete": &graphql.Field{
      Type: DestinationType,
      Description: "Delete a destination",
      Args: DestinationArgs,
      Resolve: func(params graphql.ResolveParams) (interface{}, error) {
        id, _ := params.Args["id"].(string)

        client, ctx := db.DbConnect()
        db := client.Database(DbName)
        coll := db.Collection(CollName)
        objID, err := primitive.ObjectIDFromHex(id)

        res := coll.FindOne(ctx, bson.M{"_id": objID})
        deleteRes, err := coll.DeleteOne(ctx, bson.M{"_id": objID})
        lib.LogError(err)

        delCount := deleteRes.DeletedCount

        if (delCount == 0) {
          return nil, nil
        } else {
          d := models.Destination{}
          res.Decode(&d)
          return d, nil
        }
      },
    },
  }

  rootMutation := graphql.ObjectConfig{Name: "RootMutation", Fields: fields}
  return rootMutation
}
