package gql

import (
  "../conf"

  "github.com/graphql-go/graphql"
)

var (
  DbName = conf.DB_CONFIG.DbName
  CollName = conf.DB_CONFIG.CollName

  DestinationType *graphql.Object
  DestinationArgs graphql.FieldConfigArgument
)

func init() {
  DestinationType = graphql.NewObject(graphql.ObjectConfig{
    Name: "Destination",
    Fields: graphql.Fields{
      "id": &graphql.Field{
        Type: ObjectID,
      },
      "name": &graphql.Field{
        Type: graphql.String,
      },
      "country": &graphql.Field{
        Type: graphql.String,
      },
      "warm": &graphql.Field{
        Type: graphql.Boolean,
      },
      "expensive": &graphql.Field{
        Type: graphql.Boolean,
      },
      "popularity": &graphql.Field{
        Type: graphql.Int,
      },
      "rating": &graphql.Field{
        Type: graphql.Float,
      },
    },
  })

  DestinationArgs = graphql.FieldConfigArgument{
    "id": &graphql.ArgumentConfig{
      Type: graphql.String,
    },
    "name": &graphql.ArgumentConfig{
      Type: graphql.String,
    },
    "country": &graphql.ArgumentConfig{
      Type: graphql.String,
    },
    "warm": &graphql.ArgumentConfig{
      Type: graphql.Boolean,
    },
    "expensive": &graphql.ArgumentConfig{
      Type: graphql.Boolean,
    },
    "popularity": &graphql.ArgumentConfig{
      Type: graphql.Int,
    },
    "rating": &graphql.ArgumentConfig{
      Type: graphql.Float,
    },
  }
}
