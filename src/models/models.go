package models

import (
  "go.mongodb.org/mongo-driver/bson/primitive"
)

type Destination struct {
  ID primitive.ObjectID `bson:"_id" json:"id,omitempty"`
  Name string `bson:"name" json:"name"`
  Country string `bson:"country" json:"country"`
  Warm bool `bson:"warm" json:"warm"`
  Expensive bool `bson:"expensive" json:"expensive"`
  Popularity int `bson:"popularity" json:"popularity"`
  Rating float64 `bson:"rating" json:"rating"`
}
