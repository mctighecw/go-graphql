package server

import (
  "../handlers"
  "../lib"

  "net/http"
  "time"

  "github.com/friendsofgo/graphiql"
  "github.com/gorilla/mux"
)

func CreateServer(port string) *http.Server {
  r := mux.NewRouter()

  // Custom logger
  r.Use(handlers.Logger)

  // Graphiql
  graphiqlHandler, err := graphiql.NewGraphiqlHandler("/graphql")
  lib.ErrorPanic(err)

  // Routes
  r.HandleFunc("/graphql", handlers.GraphqlHandler)
  r.Handle("/graphiql", handlers.CheckGraphiqlStatus(graphiqlHandler))

  s := &http.Server{
    Handler: r,
    Addr: port,
    WriteTimeout: 15 * time.Second,
    ReadTimeout: 15 * time.Second,
  }

  // Info
  mode := lib.GetEnv("APP_ENV", "development")
  m := "Mode: " + mode
  lib.PrintMessage(m)

  return s
}
