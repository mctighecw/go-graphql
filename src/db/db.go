package db

import (
  "../conf"
  "../lib"

  "context"
  "fmt"
  "strconv"
  "time"

  "go.mongodb.org/mongo-driver/mongo"
  "go.mongodb.org/mongo-driver/mongo/options"
)

func DbConnect() (*mongo.Client, context.Context) {
  timeout := 10*time.Second
  port := strconv.FormatInt(int64(conf.DB_CONFIG.Port), 10)
  uri := fmt.Sprintf("mongodb://%s:%s@%s:%s/?authSource=%s",
    conf.DB_CONFIG.User, conf.DB_CONFIG.Pw, conf.DB_CONFIG.Host,
    port, conf.DB_CONFIG.AdminDb)
  ctx, _ := context.WithTimeout(context.Background(), timeout)
  client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
  lib.LogError(err)

  return client, ctx
}
