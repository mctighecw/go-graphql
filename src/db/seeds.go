package db

import (
  "../models"

  "go.mongodb.org/mongo-driver/bson/primitive"
)

var (
  D1 = models.Destination{
    ID: primitive.NewObjectID(),
    Name: "Hawaii",
    Country: "USA",
    Warm: true,
    Expensive: false,
    Popularity: 1,
    Rating: 1.4,
  }

  D2 = models.Destination{
    ID: primitive.NewObjectID(),
    Name: "Amalfi",
    Country: "Italy",
    Warm: true,
    Expensive: true,
    Popularity: 2,
    Rating: 1.6,
  }

  D3 = models.Destination{
    ID: primitive.NewObjectID(),
    Name: "Alicante",
    Country: "Spain",
    Warm: true,
    Expensive: false,
    Popularity: 3,
    Rating: 1.9,
  }

  D4 = models.Destination{
    ID: primitive.NewObjectID(),
    Name: "Sylt",
    Country: "Germany",
    Warm: false,
    Expensive: true,
    Popularity: 4,
    Rating: 2.7,
  }

  D5 = models.Destination{
    ID: primitive.NewObjectID(),
    Name: "Miami Beach",
    Country: "USA",
    Warm: true,
    Expensive: true,
    Popularity: 2,
    Rating: 1.8,
  }

  D6 = models.Destination{
    ID: primitive.NewObjectID(),
    Name: "Bali",
    Country: "Indonesia",
    Warm: true,
    Expensive: false,
    Popularity: 3,
    Rating: 1.7,
  }
)
