package main

import (
  "./conf"
  "./lib"
  "./server"

  "fmt"
  "log"
)

func main() {
  p := conf.APP_CONFIG.Port
  ps := fmt.Sprintf(":%d", p)
  s := server.CreateServer(ps)

  // Server info
  n := conf.APP_CONFIG.Name
  pss := fmt.Sprintf("%d", p)
  m := "Starting " + n + " on port " + pss
  i := fmt.Sprintf(m)
  lib.PrintMessage(i)

  log.Fatal(s.ListenAndServe())
}
