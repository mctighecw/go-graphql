// See: https://ndersson.me/post/capturing_status_code_in_net_http/

package handlers

import (
  "net/http"
)

type loggingResponseWriter struct {
  http.ResponseWriter
  statusCode int
}

func (lrw *loggingResponseWriter) WriteHeader(code int) {
  lrw.statusCode = code
  lrw.ResponseWriter.WriteHeader(code)
}

func NewLoggingResponseWriter(w http.ResponseWriter) *loggingResponseWriter {
  return &loggingResponseWriter{w, http.StatusOK}
}
