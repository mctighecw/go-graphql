package handlers

import (
  "../lib"

  "fmt"
  "log"
  "net/http"
)

func Logger(h http.Handler) http.Handler {
  return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
    lrw := NewLoggingResponseWriter(w)
    h.ServeHTTP(lrw, r)
    code := lrw.statusCode
    statusCode := fmt.Sprintf("%d", code)

    log.Printf(
      "\t%s\t%s\t%s\t",
      r.Method,
      statusCode,
      r.RequestURI,
    )
  })
}

func CheckGraphiqlStatus(h http.Handler) http.Handler {
  return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
    mode := lib.GetEnv("APP_ENV", "development")
    disableGraphiql := lib.GetEnvBool("DISABLE_GRAPHIQL", true)

    if mode == "production" && disableGraphiql {
      http.Error(w, "Not allowed", http.StatusForbidden)
      return
    }
    h.ServeHTTP(w, r)
  })
}
