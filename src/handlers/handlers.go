package handlers

import (
  "../gql"

  "encoding/json"
  "fmt"
  "net/http"

  "github.com/graphql-go/graphql"
)

type ReqBody struct {
  Query string `json:"query"`
}

func GraphqlHandler(w http.ResponseWriter, r *http.Request) {
  schema := gql.CreateSchema()

  var rBody ReqBody
  err := json.NewDecoder(r.Body).Decode(&rBody)
  if err != nil {
    http.Error(w, "Error parsing JSON request body", 400)
  }

  query := rBody.Query
  params := graphql.Params{Schema: schema, RequestString: query}
  result := graphql.Do(params)

  if len(result.Errors) > 0 {
    fmt.Println("Graphql operation failed:", result.Errors)
  }

  rJSON, _ := json.Marshal(result)
  w.Write([]byte(rJSON))
}
