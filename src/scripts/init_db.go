package main

import (
  "../conf"
  "../db"
  "../lib"

  "fmt"
)

var (
  DbName = conf.DB_CONFIG.DbName
  CollName = conf.DB_CONFIG.CollName

  seeds = []interface{}{}

  s1 = db.D1
  s2 = db.D2
  s3 = db.D3
  s4 = db.D4
  s5 = db.D5
  s6 = db.D6
)

func main() {
  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollName)

  seeds = append(seeds, s1, s2, s3, s4, s5, s6)
  result, err := coll.InsertMany(ctx, seeds)

  lib.LogError(err)
  fmt.Println(result)
}
