package main

import (
  "../conf"
  "../db"
  "../lib"

  "fmt"

  "go.mongodb.org/mongo-driver/bson"
)

var (
  DbName = conf.DB_CONFIG.DbName
  CollName = conf.DB_CONFIG.CollName
)

func main() {
  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollName)

  f := bson.M{}
  res, err := coll.DeleteMany(ctx, f)

  lib.LogError(err)
  fmt.Println(res)
}
