package lib

import (
  "fmt"
  "os"
  "strconv"
)

func PrintMessage(m string) {
  fmt.Println(m)
}

func LogError(err error) {
  if err != nil {
    fmt.Println(err)
    return
  }
}

func ErrorPanic(err error) {
  if err != nil {
    panic(err)
  }
}

func GetEnv(key, fallback string) string {
  if value, ok := os.LookupEnv(key); ok {
    return value
  }
  return fallback
}

func GetEnvBool(key string, fallback bool) bool {
  s := os.Getenv(key)
  if s == "" {
    return fallback
  }

  v, err := strconv.ParseBool(s)
  if err != nil {
    return fallback
  }
  return v
}
