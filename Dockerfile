FROM golang:1.13

WORKDIR /usr/src

RUN go get -u github.com/friendsofgo/graphiql
RUN go get -u github.com/gorilla/mux
RUN go get -u github.com/graphql-go/graphql
RUN go get -u go.mongodb.org/mongo-driver/bson
RUN go get -u go.mongodb.org/mongo-driver/mongo

COPY . .

EXPOSE 9600

CMD ["go", "run", "./src/main.go"]
